"""
Obtiene un documento a partir del id de una notificacion
"""

import sys
import json


from firmamex import FirmamexServices

webid = ""
apikey = ""
notificationId = 0

services = FirmamexServices(webid, apikey)
result = services.getData({
    "notification_id":notificationId,
    "action":"download_original_bytes"
})


print(result)