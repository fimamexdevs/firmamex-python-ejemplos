"""
Ejemplo para obtener una estampilla de tiempo
"""

from firmamex import FirmamexServices
import json
import hashlib
import base64

# En caso de que se quiera obtener el hash
# de un archivo
def fileHash256(filePath):
    BLOCKSIZE = 65536
    hasher = hashlib.sha256()
    with open(filePath, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()

webid = ""
apikey = ""

services = FirmamexServices(webid, apikey)

hash256 = hashlib.sha256()
hash256.update(b"datos a estampillar")
hexHash = hash256.hexdigest()

timestamp = services.timestamp(hexHash)
validationResult = services.timestampValidate(b"datos a estampillar", base64.b64decode(timestamp['timestamp']))

print(timestamp)
print(validationResult)