"""
Ejemplo para generar un documento a partir de una URL colocando
los stickers por medio del API
"""

from firmamex import FirmamexServices

webid = ""
apikey = ""

services = FirmamexServices(webid, apikey)

response = services.request({ 
        "app2":True,
        "url_doc" : "https://www.dropbox.com/s/sxvgq1uhb4k3s4w/contrato.pdf?dl=0",        
        "stickers": [{
            "authority":"SAT",
            "stickerType":"line",
            "dataType":"rfc",
            "imageType":"desc",
            "data":"GOCF9002226A711",
            "page":0,
            "rect": {
                "lx":355,
                "ly":102,
                "tx":555,
                "ty": 202
            }
        }]
    })

print(response)